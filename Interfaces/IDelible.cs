﻿namespace StudioKit.Data.Interfaces;

public interface IDelible
{
	bool IsDeleted { get; set; }
}