﻿using System;

namespace StudioKit.Data.Interfaces;

/// <summary>
/// An entity with auditable properties, to denote with when the entity was stored, when it was last updated, and who last updated it.
/// </summary>
public interface IAuditable
{
	DateTime DateStored { get; set; }

	DateTime DateLastUpdated { get; set; }

	string LastUpdatedById { get; set; }
}