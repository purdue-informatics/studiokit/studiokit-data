﻿namespace StudioKit.Data.Interfaces;

public interface IOwnable
{
	string CreatedById { get; set; }
}