﻿using Newtonsoft.Json;
using StudioKit.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.Data;

/// <summary>
/// An abstract base class for generic Models.
/// Sets the Primary Key as the "int" <see cref="Id"/>
/// </summary>
public abstract class ModelBase : IAuditable
{
	[Key]
	public int Id { get; set; }

	[Timestamp]
	[JsonIgnore]
	[Required]
	public byte[] RowVersion { get; set; }

	/// <summary>
	/// Date the entity was stored in the database.
	/// </summary>
	[Required]
	public DateTime DateStored { get; set; }

	/// <summary>
	/// Date the entity was updated in the database.
	/// </summary>
	[Required]
	public DateTime DateLastUpdated { get; set; }

	/// <summary>
	/// Id of the last user to update the entity, or "null" if it was updated by the system.
	/// </summary>
	public string LastUpdatedById { get; set; }
}