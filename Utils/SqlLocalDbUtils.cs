﻿namespace StudioKit.Data.Utils;

public static class SqlLocalDbUtils
{
	public const string LocalDbServerPrefix = "(localdb)\\";
	public const string LocalDbPipeName = "LOCALDB";

	public static bool IsLocalDb(string connectionString)
	{
		return !string.IsNullOrWhiteSpace(connectionString) &&
				(connectionString.Contains(LocalDbServerPrefix) ||
				connectionString.Contains(LocalDbPipeName));
	}
}